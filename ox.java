import java.util.Scanner;

public class ox {

    public static void printWelcome(){
        System.out.println("Welcome to ox Game");
    }

    static String[][] tb = {{"  ", "1 ","2 ","3 "},{"1. ", "- ", "- ", "- "},{"2. ", "- ", "- ", "- "},{"3. ", "- ", "- ", "- "}};
    public static void printTable(){   	
    	for(int i=0; i<4; i++) {
    		for(int j=0; j<4; j++) {
    			System.out.print(tb[i][j]);
    		}System.out.println();
    	}
    	
    }
    
    static String player = "x ";
    public static void printTurn(){
    	if(player == "x ") {
    		player = "o ";
    		System.out.println(player + "Turn");
    	}else {
    		player = "x ";
    		System.out.println(player + "Turn");
    	}
    		
    }

    public static void Input(){
    	Scanner kb = new Scanner(System.in);
    	int row,col;
   
    	System.out.print("Please Input Row Col : ");
    	row = kb.nextInt();
    	col = kb.nextInt();
    			
    	if(tb[row][col].equals("- ")) {
    		tb[row][col] = player;
    		System.out.println();
    	}else {
    		System.out.println();
    		System.out.println("Please check your position and play agin.");
    		System.out.println();
    		if(player == "x ") {
    			player = "o ";
    		}else {
    			player = "x ";
    		}

    	}
    	
    }
    
    static boolean checkWin = false;
    static int count = 0;
    public static void checksWin(){
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(tb[j][i] == player) {
    				count++;
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(tb[i][j] == player) {
    				count++;
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(i == j) {
    				if(tb[i][j] == player) {
    					count++;
    				}	
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(i + j == 4) {
    				if(tb[i][j] == player) {
    					count++;
    				}
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    }
    
    public static void printWin(){
    	if(checkWin == true) {
    		System.out.println(player + "Win!!");
    	}else {
    		System.out.println("Draw!!");
    	}
    	
    }

    

    public static void main(String[] args){
        printWelcome();
        
        while(checkWin == false) {
        	printTable();
        	printTurn();
        	Input();
        	checksWin();
        	if(checkWin == true) {
        		break;
        	}
        }
        printTable();
        printWin();
        	
    
    }
}